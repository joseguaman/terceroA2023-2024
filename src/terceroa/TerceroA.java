/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package terceroa;


import controlador.TDA.colas.QueueUltimate;
import controlador.TDA.listas.Exception.EmptyException;
import controlador.TDA.pilas.FulStacklException;
import controlador.TDA.pilas.StackUltimate;

/**
 *
 * @author sebastian
 */
public class TerceroA {
    private Double calculo(Double a, Double b, Character operacion) {
        if(operacion.charValue() == '+')
            return a+b;
        else if(operacion.charValue() == '-')
            return a-b;
        else if(operacion.charValue() == '*')
            return a*b;
        else if(operacion.charValue() == '/')
            return a/b;
        else
            return Double.NaN;
    }
    private Boolean validadorOperacion(Character a) {
        return (a.charValue() == '+' || a.charValue() == '-'
                ||a.charValue() == '*'||a.charValue() == '/');
    }
    public Double calcular(String info) throws EmptyException, FulStacklException {
        StackUltimate<Double> pila = new StackUltimate<>(10);
        Double resp = Double.NaN;
        for(Character a:info.toCharArray()) {
            if(Character.isDigit(a)) {
                pila.push(Double.parseDouble(a.toString()));
            }else {
                if(validadorOperacion(a)) {
                    Double op1 = pila.pop();
                    Double op2 = pila.pop();
                    pila.push(calculo(op1, op2, a));
                }
            }
        }
        if(pila.length() == 1)
            resp=pila.pop();
        
        return resp;
    }
    public static void imprimir(Integer[] m) {
        System.out.println("********");
        for (int i = 0; i < m.length; i++) {
            System.out.print(m[i] + "\t");
        }
        System.out.println("********");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*String info = "64*46+-6";
        TerceroA ta = new TerceroA();
        try {
            Double resp = ta.calcular(info);
            if(resp.isNaN())
                System.out.println("No es una opreacion valida");
            else
                System.out.println(resp);
        } catch (Exception e) {
            System.out.println("Error "+e.toString());
        }*/
        //shell  --> quicksort
         long tiempoInicio;
         long tiempoFinal;
        Integer[] m = new Integer[5000];
        Integer[] sac = new Integer[5000];
        Integer[] in = new Integer[5000];
        Integer[] se = new Integer[5000];
        for (int i = 0; i < m.length; i++) {
            Integer aux = (int) (Math.random() * 100);
            m[i] = aux;
            in[i] = aux;
            se[i] = aux;
            sac[i] = aux;
        }
        TerceroA.imprimir(m);
        Integer intercambios = 0;
        //burbuja
        tiempoInicio = System.nanoTime();
        for (int i = 1; i < m.length - 1; i++) {
            for (int j = m.length - 1; j > 0; j--) {
                if (m[j - 1] > m[j]) {
                    Integer aux = m[j - 1];
                    m[j - 1] = m[j];
                    m[j] = aux;
                    intercambios++;
                }
            }
        }
        tiempoFinal = System.nanoTime();
        TerceroA.imprimir(m);
        System.out.println("Intrercambios burbuja " + intercambios);
        System.out.println("Tiempo Burbuja: " + (tiempoFinal-tiempoInicio) + " nanosegundos.");
        //insercion
        tiempoFinal = 0;
        tiempoFinal = 0;
        System.out.println("INSERSION");
        TerceroA.imprimir(in);
        Integer intercambioI = 0;
        Integer n = in.length;
        tiempoInicio = System.nanoTime();
        for (int i = 1; i < n; i++) {
            int j = i - 1;
            Integer t = in[i];
            while (j >= 0 && t < in[j]) {
                in[j + 1] = in[j];
                j = j - 1;
                intercambioI++;
            }
            in[j + 1] = t;
        }
        tiempoFinal = System.nanoTime();
        TerceroA.imprimir(in);
        System.out.println("Intrercambios insercion " + intercambioI);
        System.out.println("Tiempo insercion: " + (tiempoFinal-tiempoInicio) + " nanosegundos.");
        //SELECCION
        System.out.println("SELECCION");
        TerceroA.imprimir(se);
        Integer intercambioSe = 0;
        tiempoFinal = 0;
        tiempoFinal = 0;
        tiempoInicio = System.nanoTime();
        for (int i = 0; i < n - 1; i++) {
            int k = i;
            int t = se[i];
            for (int j = i + 1; j < n; j++) {
                if (se[j] < t) {
                    t = se[j];
                    k = j;
                    intercambioSe++;
                }
            }
            se[k] = se[i];
            se[i] = t;
        }
        tiempoFinal = System.nanoTime();
        TerceroA.imprimir(in);
        System.out.println("Intrercambios seleccion " + intercambioSe);
        System.out.println("Tiempo seleccion: " + (tiempoFinal-tiempoInicio) + " nanosegundos.");
        
        
        //sacudida
        System.out.println("SACUDIDA");
        TerceroA.imprimir(sac);
        Integer intercambioSac = 0;
        int i,j,izq,der,aux = 0;
        izq = 1;
        der = n - 1;
        j = n - 1;
        tiempoFinal = 0;
        tiempoFinal = 0;
        tiempoInicio = System.nanoTime();
        do{
            for(i = der;i >=izq; i--) {
                if(sac[i-1]>sac[i]) {
                    aux = sac[i];
                    sac[i] = sac[i-1];
                    sac[i-1] = aux;
                    j = i;
                    intercambioSac++;
                }
            }
            izq = j+1;
            for(i = izq;i <=der; i++) {
                if(sac[i-1]>sac[i]) {
                    aux = sac[i];
                    sac[i] = sac[i-1];
                    sac[i-1] = aux;
                    j = i;
                    intercambioSac++;
                }
            }
            der = j-1;
        }while (izq <= der);
        tiempoFinal = System.nanoTime();
        TerceroA.imprimir(sac);
        System.out.println("Intrercambios seleccion " + intercambioSac);
        System.out.println("Tiempo sacudida: " + (tiempoFinal-tiempoInicio) + " nanosegundos.");
        
    }
    
}




