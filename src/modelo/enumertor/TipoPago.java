/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.enumertor;

/**
 *
 * @author sebastian
 */
public enum TipoPago {
    EFECTIDO, TARJETA_CREDITO, DIFERIDO
}
