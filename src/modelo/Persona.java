/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author sebastian
 */
public class Persona {

    private Integer id;
    private String apellido;
    private String nombre;
    private String fono;
    private String direccion;
    private String dni;
    private Integer id_rol;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getId_rol() {
        return id_rol;
    }

    public void setId_rol(Integer id_rol) {
        this.id_rol = id_rol;
    }

    @Override
    public String toString() {
        return apellido + " " + nombre; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Boolean compare(Persona p, String field, Integer type) {
        //0 menor    1 mayor
        switch (type) {
            case 0:
                if (field.equalsIgnoreCase("apellido")) {
                    return apellido.compareTo(p.getApellido()) < 0;
                } else if (field.equalsIgnoreCase("nombre")) {
                    return nombre.compareTo(p.getNombre()) < 0;
                } else if (field.equalsIgnoreCase("dni")) {
                    return dni.compareTo(p.getDni()) < 0;
                } else if (field.equalsIgnoreCase("direccion")) {
                    return direccion.compareTo(p.getDireccion()) < 0;
                } else if (field.equalsIgnoreCase("id")) {
                    return (id.intValue() < p.getId().intValue());
                }
                //break;
            case 1:
                if (field.equalsIgnoreCase("apellido")) {
                    return apellido.compareTo(p.getApellido()) > 0;
                } else if (field.equalsIgnoreCase("nombre")) {
                    return nombre.compareTo(p.getNombre()) > 0;
                } else if (field.equalsIgnoreCase("dni")) {
                    return dni.compareTo(p.getDni()) > 0;
                } else if (field.equalsIgnoreCase("direccion")) {
                    return direccion.compareTo(p.getDireccion()) > 0;
                } else if (field.equalsIgnoreCase("id")) {
                    return (id.intValue() > p.getId().intValue());
                }
                //break;
            default:
                return null;
        }

    }

}
