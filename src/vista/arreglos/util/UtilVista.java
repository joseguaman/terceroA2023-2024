/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.arreglos.util;

import controlador.RolControl;
import controlador.TDA.listas.Exception.EmptyException;
import javax.swing.JComboBox;
import modelo.Rol;

/**
 *
 * @author sebastian
 */
public class UtilVista {
    public static void cargarcomboRolesL(JComboBox cbx) throws EmptyException {
        controlador.personas.RolControl rc = new controlador.personas.RolControl();        
        cbx.removeAllItems();
        if(rc.getListR().isEmpty())
            throw new EmptyException("No hay roles que mostrar");
        
        for(int i = 0; i < rc.getListR().getLength(); i++) {
            cbx.addItem(rc.getListR().getInfo(i));
        }
    }
    public static Rol obtenerRolControlL(JComboBox cbx) {
        return (Rol)cbx.getSelectedItem();
    }
    
    //NO VALEN
    public static void cargarcomboRoles(JComboBox cbx) {
        RolControl rc = new RolControl();        
        cbx.removeAllItems();
        for(int i = 0; i < rc.getRoles().length; i++) {
            cbx.addItem(rc.getRoles()[i]);
        }
    }
    public static Rol obtenerRolControl(JComboBox cbx) {
        return (Rol)cbx.getSelectedItem();
    }  
    
}













