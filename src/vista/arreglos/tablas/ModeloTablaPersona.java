/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.arreglos.tablas;

import javax.swing.table.AbstractTableModel;
import modelo.Persona;

/**
 *
 * @author sebastian
 */
public class ModeloTablaPersona extends AbstractTableModel {
    private Persona personas[];

    public Persona[] getPersonas() {
        return personas;
    }

    public void setPersonas(Persona[] personas) {
        this.personas = personas;
    }
        
    @Override
    public int getRowCount() {
        // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return personas.length;
    }

    @Override
    public int getColumnCount() {
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         return 4;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        //i = fila     i1 = columna
        Persona p = personas[i];
        switch (i1) {
            case 0:
                /*if(p != null)
                    return p.getDni();
                else
                    return "";*/
                return (p != null) ? p.getDni() : "";
            case 1: return (p != null) ? p.getApellido()+" "+p.getNombre() : "";
            case 2: return (p != null) ? p.getFono() : "";
            case 3: return (p != null) ? p.getDireccion() : "";                
                
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: return "DNI";
            case 1: return "USUARIO";
            case 2: return "TELEFONO";
            case 3: return "DIRECCION";                
                
            default:
                return null;
        }
    }
    
    
    
}
