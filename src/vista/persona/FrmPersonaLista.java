/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista.persona;

import controlador.personas.PersonaControl;
import controlador.utiles.Utiles;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.*;

import javax.swing.JOptionPane;
import vista.arreglos.tablas.ModeloTablaPersona;
import vista.arreglos.util.UtilVista;
import vista.lista.tablas.ModeloTablaPersonaLista;

/**
 *
 * @author sebastian
 */
public class FrmPersonaLista extends javax.swing.JDialog {

    //private PersonaControl personaControl = new PersonaControl(10);
    //private ModeloTablaPersona mtp = new ModeloTablaPersona();
    private PersonaControl personaControl = new PersonaControl();
    private ModeloTablaPersonaLista mtp = new ModeloTablaPersonaLista();

    /**
     * Creates new form FrmPersona
     */
    public FrmPersonaLista(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        limpiar();

    }
    
    private void buscar() {
        String texto = txt_buscar.getText();
        String criterio = cbxCriterio1.getSelectedItem().toString();
        try {
            if(criterio.equalsIgnoreCase("apellido"))
                mtp.setPersonas(personaControl.buscarApellidos(texto, personaControl.getListP()));
            else if(criterio.equalsIgnoreCase("nombre"))
                mtp.setPersonas(personaControl.buscarNombres(texto, personaControl.getListP()));
            tbltabla.setModel(mtp);
            tbltabla.updateUI();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(),
                    "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void ordenar() {
        String criterio = cbxCriterio.getSelectedItem().toString();
        Integer tipo = 0;
        if (btn_tipo.isSelected()) {
            tipo = 1;
        }
        try {
            mtp.setPersonas(personaControl.ordenar(personaControl.getListP(), tipo, criterio));
            tbltabla.setModel(mtp);
            tbltabla.updateUI();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(),
                    "ERROR", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void cargarTabla() {
        mtp.setPersonas(personaControl.getListP());
        tbltabla.setModel(mtp);
        tbltabla.updateUI();
    }

    private Boolean validar() {
        return (!txtapellidos.getText().trim().isEmpty() && !txtdir.getText().trim().isEmpty()
                && !txtdni.getText().trim().isEmpty());
    }

    private void limpiar() {
        try {
            UtilVista.cargarcomboRolesL(cbxrol);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(),
                    "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        txtdni.setEnabled(true);
        tbltabla.clearSelection();
        txtdni.setText("");
        txtapellidos.setText("");
        txtdir.setText("");
        txtemial.setText("");
        txtfono.setText("");
        txtnombres.setText("");
        cargarTabla();
        personaControl.setPersona(null);
        //cbxrol.setSelectedIndex(0);
    }

    //persona ----> memoria  0xFAA
    //[          ]
    //[persona,         ]
    //[persona, persona       ]
    private void guardar() {
        if (validar()) {

            if (Utiles.idCardEcuador(txtdni.getText())) {
                personaControl.getPersona().setDni(txtdni.getText());
                personaControl.getPersona().setApellido(txtapellidos.getText());
                personaControl.getPersona().setDireccion(txtdir.getText());
                personaControl.getPersona().setFono(txtfono.getText());
                personaControl.getPersona().setNombre(txtnombres.getText());
                personaControl.getPersona().setId_rol(UtilVista.obtenerRolControl(cbxrol).getId());
                if (personaControl.persit()) {
                    JOptionPane.showMessageDialog(null, "Daros guardados",
                            "OK", JOptionPane.INFORMATION_MESSAGE);
                    //personaControl.imprimir();
                    limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo guardar, hubo un error!",
                            "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error, cedula no valida!",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Falta llenar campos",
                    "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void cargarVista() {
        int fila = tbltabla.getSelectedRow();
        if (fila < 0) {
            JOptionPane.showMessageDialog(null, "Escoja un registro de la tabla",
                    "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                personaControl.setPersona(mtp.getPersonas().getInfo(fila));
                txtapellidos.setText(personaControl.getPersona().getApellido());
                txtdir.setText(personaControl.getPersona().getDireccion());
                txtdni.setText(personaControl.getPersona().getDni());
                txtdni.setEnabled(false);
                txtemial.setText("");
                txtfono.setText(personaControl.getPersona().getFono());
                txtnombres.setText(personaControl.getPersona().getNombre());
                cbxrol.setSelectedIndex(personaControl.getPersona().getId_rol() - 1);
            } catch (Exception e) {
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdni = new javax.swing.JTextPane();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtapellidos = new javax.swing.JTextPane();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtnombres = new javax.swing.JTextPane();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtemial = new javax.swing.JTextPane();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtfono = new javax.swing.JTextPane();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtdir = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cbxrol = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbltabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        cbxCriterio = new javax.swing.JComboBox<>();
        btn_tipo = new javax.swing.JCheckBox();
        jButton5 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txt_buscar = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        cbxCriterio1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("ADMINISTRAR PERSONAS");
        getContentPane().setLayout(null);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(null);

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setLayout(null);

        jLabel1.setText("DNI");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(20, 20, 80, 19);

        jScrollPane1.setViewportView(txtdni);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(130, 20, 160, 24);

        jLabel2.setText("APELLIDOS");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(20, 60, 90, 19);

        jScrollPane2.setViewportView(txtapellidos);

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(130, 60, 160, 24);

        jLabel3.setText("ROLES");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(300, 20, 90, 19);

        jScrollPane3.setViewportView(txtnombres);

        jPanel2.add(jScrollPane3);
        jScrollPane3.setBounds(410, 60, 160, 24);

        jLabel4.setText("CORREO");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(20, 100, 90, 19);

        jScrollPane4.setViewportView(txtemial);

        jPanel2.add(jScrollPane4);
        jScrollPane4.setBounds(130, 100, 160, 24);

        jLabel5.setText("CELULAR");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(300, 100, 90, 19);

        jScrollPane5.setViewportView(txtfono);

        jPanel2.add(jScrollPane5);
        jScrollPane5.setBounds(410, 100, 160, 24);

        jLabel6.setText("DIRECCION");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(20, 140, 90, 19);

        txtdir.setColumns(20);
        txtdir.setLineWrap(true);
        txtdir.setRows(5);
        txtdir.setWrapStyleWord(true);
        jScrollPane6.setViewportView(txtdir);

        jPanel2.add(jScrollPane6);
        jScrollPane6.setBounds(130, 140, 440, 70);

        jButton2.setText("GUARDAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(160, 230, 100, 25);

        jButton3.setText("CANCELAR");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3);
        jButton3.setBounds(310, 230, 110, 25);

        jLabel7.setText("NOMBRES");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(300, 60, 90, 19);

        cbxrol.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cbxrol);
        cbxrol.setBounds(410, 20, 160, 25);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 590, 270);

        jPanel3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel3.setLayout(null);

        tbltabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane7.setViewportView(tbltabla);

        jPanel3.add(jScrollPane7);
        jScrollPane7.setBounds(10, 140, 560, 160);

        jLabel8.setText("Criterio");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(20, 30, 70, 19);

        cbxCriterio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DNI", "APELLIDO", "NOMBRE" }));
        jPanel3.add(cbxCriterio);
        cbxCriterio.setBounds(90, 30, 140, 25);

        btn_tipo.setText("Descencente");
        jPanel3.add(btn_tipo);
        btn_tipo.setBounds(250, 30, 120, 23);

        jButton5.setText("ORDENAR");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton5);
        jButton5.setBounds(400, 30, 110, 25);

        jLabel9.setText("Texto");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(20, 80, 35, 19);
        jPanel3.add(txt_buscar);
        txt_buscar.setBounds(240, 80, 150, 25);

        btn_buscar.setText("BUSCAR");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });
        jPanel3.add(btn_buscar);
        btn_buscar.setBounds(400, 80, 100, 25);

        cbxCriterio1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DNI", "APELLIDO", "NOMBRE" }));
        jPanel3.add(cbxCriterio1);
        cbxCriterio1.setBounds(90, 80, 140, 25);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 290, 590, 310);

        jButton1.setText("SALIR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(470, 610, 110, 25);

        jButton4.setText("Escoger");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(30, 610, 130, 25);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 610, 650);

        setSize(new java.awt.Dimension(610, 691));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        limpiar();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        cargarVista();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        ordenar();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_btn_buscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPersonaLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPersonaLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPersonaLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPersonaLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmPersonaLista dialog = new FrmPersonaLista(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    private javax.swing.JCheckBox btn_tipo;
    private javax.swing.JComboBox<String> cbxCriterio;
    private javax.swing.JComboBox<String> cbxCriterio1;
    private javax.swing.JComboBox<String> cbxrol;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTable tbltabla;
    private javax.swing.JTextField txt_buscar;
    private javax.swing.JTextPane txtapellidos;
    private javax.swing.JTextArea txtdir;
    private javax.swing.JTextPane txtdni;
    private javax.swing.JTextPane txtemial;
    private javax.swing.JTextPane txtfono;
    private javax.swing.JTextPane txtnombres;
    // End of variables declaration//GEN-END:variables
}
