/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package controlador.TDA.colas;


import controlador.TDA.listas.Exception.EmptyException;
import controlador.TDA.pilas.FulStacklException;

/**
 *
 * @author sebastian
 */
public class QueueUltimate<E> {
    private Queue<E> tail;

    public QueueUltimate(Integer length) {
        this.tail = new Queue<>(length);
    }
    
    public void queue(E info) throws EmptyException, FulStacklException {
        tail.queue(info);
    }
    
    public E dequeue() throws EmptyException {
        return tail.dequeue();
    }    
    public Integer length() {
        return tail.getLength();
    }
    public Boolean isFull() {
        return tail.isFull();
    }
    public void print() {
        System.out.println("Queue");
        System.out.println(tail.toString());
        System.out.println("");
    }
}







