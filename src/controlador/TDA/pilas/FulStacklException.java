/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package controlador.TDA.pilas;

/**
 *
 * @author sebastian
 */
public class FulStacklException extends Exception {

    /**
     * Creates a new instance of <code>FulStacklException</code> without detail
     * message.
     */
    public FulStacklException() {
    }

    /**
     * Constructs an instance of <code>FulStacklException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public FulStacklException(String msg) {
        super(msg);
    }
}
