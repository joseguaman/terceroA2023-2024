/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.personas;

import controlador.TDA.listas.DynamicList;
import controlador.TDA.listas.Exception.EmptyException;
import controlador.dao.DaoImplement;
import controlador.utiles.Utiles;
import java.lang.reflect.Field;

import modelo.Persona;


/**
 *
 * @author sebastian
 */
public class PersonaControl extends DaoImplement<Persona> {

    private DynamicList<Persona> listP = new DynamicList<>();
    private Persona persona;

    public PersonaControl() {
        super(Persona.class);
    }

    public DynamicList<Persona> getListP() {
        listP = all();
        return listP;
    }

    public void setListP(DynamicList<Persona> listP) {
        this.listP = listP;
    }

    public Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }
        return persona;
    }

    public void setPersona(Persona rol) {
        this.persona = rol;
    }

    public Boolean persit() {
        persona.setId(all().getLength() + 1);
        return persist(persona);
    }

    public DynamicList<Persona> ordenar(DynamicList<Persona> lista, Integer tipo, String field) throws EmptyException, Exception {
        Field attribute = Utiles.getField(Persona.class, field);
        Integer n = lista.getLength();
        Persona[] personas = lista.toArray();
        if (attribute != null) {
            for (int i = 0; i < n - 1; i++) {
                int k = i;
                Persona t = personas[i];
                for (int j = i + 1; j < n; j++) {

                    //if (personas[j].getApellido().compareTo(t.getApellido()) > 0) {
                    if (personas[j].compare(t, field, tipo)) {
                        t = personas[j];
                        k = j;
                    }

                }
                personas[k] = personas[i];
                personas[i] = t;
            }
        } else
            throw new Exception("No existe el criterio de busqueda");

        return lista.toList(personas);
    }

    public DynamicList<Persona> buscarApellidos(String texto, DynamicList<Persona> personas) {
        DynamicList<Persona> lista = new DynamicList<>();
        try {
            Persona[] aux = ordenar(personas, 0, "apellido").toArray();
            for(Persona p: aux) {
                if(p.getApellido().toLowerCase().contains(texto.toLowerCase())) {
                    lista.add(p);
                }
            }
        } catch (Exception e) {
            System.out.println("Error en buscar "+e.getMessage());
        }
        return lista;
    }
    
    public DynamicList<Persona> buscarNombres(String texto, DynamicList<Persona> personas) {
        DynamicList<Persona> lista = new DynamicList<>();
        try {
            Persona[] aux = ordenar(personas, 0, "nombre").toArray();
            for(Persona p: aux) {
                if(p.getNombre().toLowerCase().contains(texto.toLowerCase())) {
                    lista.add(p);
                }
            }
        } catch (Exception e) {
            System.out.println("Error en buscar "+e.getMessage());
        }
        return lista;
    }
   

}
