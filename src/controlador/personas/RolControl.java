/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.personas;

import controlador.TDA.listas.DynamicList;
import controlador.dao.DaoImplement;
import modelo.Rol;

/**
 *
 * @author sebastian
 */
public class RolControl extends DaoImplement<Rol>{
    private DynamicList<Rol> listR = new DynamicList<>();
    private Rol rol;

    public RolControl() {
        super(Rol.class);
    }
    
    public DynamicList<Rol> getListR() {
        listR = all();
        return listR;
    }

    public void setListR(DynamicList<Rol> listR) {
        this.listR = listR;
    }

    public Rol getRol() {
        if(rol == null)
            rol = new Rol();
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    public Boolean persit() {
        rol.setId(all().getLength()+1);
        return persist(rol);
    }
    
    public static void main(String[] args) {
        RolControl rc = new RolControl();
        System.out.println(rc.all().toString());
//        rc.getRol().setDescripcion("Administrador");
//        rc.getRol().setNombre("admin");
//        rc.persit();
//        rc.setRol(null);
//        rc.getRol().setDescripcion("Cajero");
//        rc.getRol().setNombre("cash");
//        rc.persit();
//        rc.setRol(null);
//        rc.getRol().setDescripcion("Cliente");
//        rc.getRol().setNombre("client");
//        rc.persit();
//        rc.setRol(null);
    }
    
}
